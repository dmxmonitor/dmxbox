#include <DMXSerial.h>

//Détail des trames
//Réception
//Demande de mise à jour d'un canal         0xC 0xCANAL 0xVALUE
//Demande de version                        0xD
//Envoi
//Envoi d'un status prêt                    0xA
//Envoi d'un status de mise à jour réussie  0xE
//Envoi de la version firmware              0xD VERSION


#define FIRMWARE_VERSION "DMXBOX V0.0.2" 

unsigned int receiveState = 0;
unsigned int channel = 0;

//Modifie un canal DMX512
void setCanal(unsigned int number, unsigned int value){
  DMXSerial.write(number, value);
  Serial.write(0xE);
}

//Initialisation
void setup() {
  Serial.begin(9600);
  DMXSerial.init(DMXController);
  Serial.write(0xA);
}

//Interception des commandes de canaux
void loop() {
  if(Serial.available())
  {
    unsigned int received = Serial.read();
    if(received == 0xD && receiveState == 0) {
      Serial.write(0xD);
      Serial.println(FIRMWARE_VERSION);
    }
    //Demande de paramétrage canal
    else if(received == 0xC && receiveState == 0) receiveState = 1;
    else if(receiveState == 1) {
      channel = received;
      receiveState = 2;
    }
    else if(receiveState == 2){
      setCanal(channel, received);      
      receiveState = 0;
      channel = 0;
    }
  }
}
